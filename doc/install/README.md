# Installing GitLab Runner

Below we describe the one-time steps you need to perform to set up a new
gitlab-runner instance, after [downloading](downloads.md) and installing the
GitLab Runner omnibus package.

## Ubuntu 12.04, 14.04

```
# Create the user that will run your builds
sudo useradd -s /bin/false -m -r gitlab-runner

# Register your runner instance with a GitLab CI Coordinator
sudo /opt/gitlab-runner/bin/setup -C /home/gitlab-runner

# Install and start the gitlab-runner Upstart script
sudo cp /opt/gitlab-runner/doc/install/upstart/gitlab-runner.conf /etc/init/
sudo service gitlab-runner start
```

The runner will write log messages via Upstart to
`/var/log/upstart/gitlab-runner.log`.

## Centos 6

MR welcome. This would need an alternative Upstart config file (because the one
currently in the repository is not compatible with the Upstart version in
Centos 6).

## Centos 7

MR welcome. This would need a SystemD unit file.
[Example](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/1d08929f1f77e719fb360e6f443048ae141880e8/files/gitlab-cookbooks/runit/files/default/gitlab-runsvdir.service).

## Debian 7

MR welcome. This would need an init script or maybe `apt-get install runit`
plus a few Runit scripts (`run`, `log/run`).
