# GitLab Runner downloads

- Ubuntu 12.04 64-bit [gitlab-runner_5.1.0~pre.omnibus.1-1_amd64.deb](https://s3-eu-west-1.amazonaws.com/downloads-packages/ubuntu-12.04/gitlab-runner_5.1.0~pre.omnibus.1-1_amd64.deb) SHA256 064fdecf5b6056c14331ac1b37e19dccf51e629251c3a4924d4da1431ebebe25
- Ubuntu 14.04 64-bit [gitlab-runner_5.1.0~pre.omnibus.1-1_amd64.deb](https://s3-eu-west-1.amazonaws.com/downloads-packages/ubuntu-14.04/gitlab-runner_5.1.0~pre.omnibus.1-1_amd64.deb) SHA256 d226a4ff384b55e7535a913b308d763b316b8a3fa8f49f09c91d7fd3eb65f238
- Debian 7 64-bit [gitlab-runner_5.1.0~pre.omnibus.1-1_amd64.deb](https://s3-eu-west-1.amazonaws.com/downloads-packages/debian-7.8/gitlab-runner_5.1.0~pre.omnibus.1-1_amd64.deb) SHA256 95711e655a3289216864ecbe54229e76d80e8e35a4ad6e4efbcce79a83d0df67
- Enterprise Linux 6 64-bit [gitlab-runner-5.1.0~pre.omnibus.1-1.x86_64.rpm](https://s3-eu-west-1.amazonaws.com/downloads-packages/centos-6.6/gitlab-runner-5.1.0~pre.omnibus.1-1.x86_64.rpm) SHA256 2abdf72331a191c48601c5706137a5d43c70d47cb706abb994142351989090af
- Enterprise Linux 7 64-bit [gitlab-runner-5.1.0~pre.omnibus.2-1.x86_64.rpm](https://s3-eu-west-1.amazonaws.com/downloads-packages/centos-7.0.1406/gitlab-runner-5.1.0~pre.omnibus.2-1.x86_64.rpm) SHA256 df8d521e5e8c7f1fa28ced3cd9963c1020302947c29be3cd0a90c5a7e2c04ae1
